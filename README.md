# Proyecto de Luis Rangel.

El proyecto fue generado con [LoopBack](http://loopback.io) que opera sobre nodejs.

Requerimientos del sistema:

1. Se necesita tener instalado nodejs. La última versión es compatible.
2. MySQL 5.0 o mayor de forma local.

Instalación:

1. Clonar el proyecto desde este repositorio a su ambiente local.
2. Dentro del directorio del proyecto ejecutar npm install para instalar todas las dependencias del proyecto marcadas en package.json.
3. Cargar el dump de la base de datos incluida en la raíz. 
4. Ajustar las credenciales de la base en server/datasources.json.
5. Ejecutar node . para iniciar el servidor y explorador del API.
6. En la consola se indica el explorer del api http://localhost:3000/explorer
7. Para hacer cualquier movimiento al API se necesita usar http://localhost:3000/api/tienda. Para esto se puede usar el programa Postman o cualquier herramienta para generar peticiones http.
8. Para detener el servidor control+c.

Notas:

1. Actualmente el API no cuenta con autenticación para los metodos de escritura. Es posible su implementación.